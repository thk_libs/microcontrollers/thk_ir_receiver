# **IR Empfänger**

Mit dieser Klasse werden die Empfangenen Befehle von einer Infrarot Fernbedienung zurück gegeben. 
Sie dient als erleichterte Bedienung der IRRemote.h Bibliothek.

|**Button** | **Signal Kodierung** | |**Button** | **Signal Kodierung** |
|:---|:---:|:---|:---:|:---:|
|Power Button | 69 | | 0 Button | 22 |
|VOL+ Button | 70 | |1 Button | 12 |
|Func Button | 71 | |2 Button | 24 |
|Rewind Button | 68 | |3 Button | 94 |
|Play/Pause Button | 64 | |4 Button | 8 |
|Forward Button | 67 | |5 Button | 28 |
|Down Button | 7 | |6 Button | 90 |
|VOL- Button | 21  | |7 Button | 66 |
|UP Button | 9 | |8 Button | 82 |
|EQ Button | 25 | |9 Button | 74 |
|ST/REPT Button | 13 | | | |

## **Voraussetzung**

- [IRremote](https://github.com/Arduino-IRremote/Arduino-IRremote) by Armin Joachimsmeyer (Version: 4.2.0)

## **Installation**

- Um diese Klasse verwenden zu können, muss dieses Repository geklont und in das Libraries-Verzeichnis der Arduino-IDE kopiert werden.

## **Anwendung**

Zur Verwendung siehe zunächst das Beispiel `IRReceiver.ino`

**Einbinden der Bibliothek:**
```arduino
#include <thk_ir_controller.h>
```

**Instanziieren:**
```arduino
thk_IrController ir_sensor(IR_PIN);
```

**Aufrufen der Methoden:**
- Um den IR-Empfänger zu initialisieren wird folgende Methode im `void setup()` ausgeführt: `ir_sensor.begin()`
- Für das Empfangen der Signale wird folgende Methode verwendet: `ir_sensor.receive_command()`


## **Anmerkung**
Für die Verwendung der [IRremote](https://github.com/Arduino-IRremote/Arduino-IRremote) Bibliothek zusammen mit einem Buzzer, muss der Timer, welche die [IRremote](https://github.com/Arduino-IRremote/Arduino-IRremote) Bibliothek verwendet, angepasst werden.

**Timer ändern:**

In der [IRremote](https://github.com/Arduino-IRremote/Arduino-IRremote) Bibliothek im "/src/private"-Ordner den Timer (hier für den Arduino Mega) in der Datei "IRTimer.hpp" von Timer2 auf Timer3 durch Ein- & Auskommentieren ändern.

![Timer anpassen](doku/Timer_Anpassung.png)
