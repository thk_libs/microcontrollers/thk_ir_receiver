#ifndef IR_CONTROLLER_H
#define IR_CONTROLLER_H

/* ****************************************** *
 * IR Remote                                  *
 *                                            *
 * 	Button 		      |   Signal Kodierung    *
 * ------------------------------------------ *
 * Power Button		  |       69              *
 * VOL+ Button		  | 	  70              *
 * Func Button		  |       71              *
 * Rewind Button	  | 	  68              *
 * Play/Pause Button  |	   	  64              *
 * Forward Button	  | 	  67              *
 * Down Button		  | 	  7               *
 * VOL- Button	      | 	  21              *
 * UP Button		  |	      9               *
 * 0 Button	          | 	  22              *
 * EQ Button		  | 	  25              *
 * ST/REPT Button	  | 	  13              *
 * 1 Button	          |       12              *
 * 2 Button	          |       24              *
 * 3 Button	          | 	  94              *
 * 4 Button	          | 	  8               *
 * 5 Button	          | 	  28              *
 * 6 Button	          | 	  90              *
 * 7 Button	          | 	  66              *
 * 8 Button	          | 	  82              *
 * 9 Button	          | 	  74              *
 * ****************************************** */

#include <IRremote.hpp> // include the library

//#define DECODE_NEC

class thk_IrController
{
public:
    thk_IrController(const uint8_t DATA_PIN){
        IRrecv ir_receiver(DATA_PIN);
    };

    void begin(const uint8_t DATA_PIN)
    {
        ir_receiver.begin(DATA_PIN, DISABLE_LED_FEEDBACK);
    };

    int receive_command()
    {
        uint8_t ir_command = 0;
        if (ir_receiver.decode())
        {
            ir_command = ir_receiver.decodedIRData.command;
            //ir_command = ir_receiver.decodedIRData.decodedRawData;
            ir_receiver.resume();
        }
        return ir_command;
    };
       
private:
    IRrecv ir_receiver;
};


#endif