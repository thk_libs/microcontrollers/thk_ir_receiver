#include "thk_ir_controller.h"

const uint8_t IR_PIN = 46;
thk_IrController ir_sensor(IR_PIN);
uint8_t command;

void setup(){
    Serial.begin(115200);
    ir_sensor.begin(IR_PIN);
}

void loop(){
  command = ir_sensor.receive_command();
  if (command != 0){
    Serial.println(command);
  }
}


